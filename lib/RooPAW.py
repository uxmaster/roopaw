# -*- coding: utf-8 -*-
"""
********************************************************************************
  RooPAW — ROOT based Physics Analysis Workstation
  http://belle2.ifj.edu.pl/roopaw/

  Copyright (c) 2010-2023 Jacek Stypuła
  (see LICENSE for details)
********************************************************************************
"""

import sys, glob, re, os
PY3 = sys.version_info > (3,)

from IPython import get_ipython, InteractiveShell
_ip = get_ipython()
OLD_IP = hasattr(_ip.input_transformer_manager, 'push')

PAWver = os.environ.get('PAWver')
PAWbatch = os.environ.get('PAWbatch')
PAWscript = os.environ.get('PAWscript')
PAWnotebook = os.environ.get('PAWnotebook') or str(_ip).count('ipykernel')

CSI="\x1B["
if PAWbatch:
	print("__________________________________")
	print("RooPAW " + PAWver + " batch mode")
	sys.argv.append( '-b' )
elif PAWnotebook:
	print("RooPAW " + PAWver + " notebook")
else:
	print(" " + CSI + "37;44m" + "                                             "  + CSI + "0m")
	print(" " + CSI + "37;44m" + "      WELCOME to RooPAW " + PAWver + "      "  + CSI + "0m" + CSI + "47m" + "  " + CSI + "0m")
	print(" " + CSI + "37;44m" + "                                             "  + CSI + "0m" + CSI + "47m" + "  " + CSI + "0m")
	print("   " + CSI + "47m" + "                                             "  + CSI + "0m")

print("-> importing ROOT modules...")
import ROOT
from ROOT import TChain, TCanvas, TH1F, TTree, TFile, gDirectory, gStyle, TF1, TMath, gROOT, gPad, kRed, kGreen, kBlue, kYellow, TGraph
from ROOT import SetMemoryPolicy, kMemoryStrict
SetMemoryPolicy(kMemoryStrict)

# --- displayhook
def _displayhook(*args):
	ROOT.gInterpreter.EndOfLineAction()
_ip.set_hook('pre_prompt_hook', _displayhook)

if PAWbatch:
	gROOT.SetBatch()

gStyle.SetCanvasColor(0)
gStyle.SetCanvasBorderMode(0)
gStyle.SetPadColor(0)
#gStyle.SetPadBorderMode(0)
gStyle.SetStatColor(0)
gStyle.SetStatBorderSize(1)
gStyle.SetTitleFillColor(0)
gStyle.SetTitleBorderSize(1)
gStyle.SetFrameFillColor(0)
gStyle.SetFrameBorderMode(0)

if not PAWnotebook:
	print("-> opening canvas...")
	default_canvas = TCanvas("canvas", "RooPAW canvas", 600, 600)
else:
	import rootnotes
	default_canvas = rootnotes.canvas("canvas", (600, 600))
	default_canvas.SetCanvasSize(512, 512)

default_canvas.plots = 0
default_canvas.tmp = {}
PAWsavedNS = []


#------ core functions/classes
def increment_plots(canvas):
	canvas = gPad.GetCanvas()
	pads = PAWpads(canvas)
	if pads < 1:
		pads = 1
	canvas.plots += 1
	canvas.plots %= pads
	return canvas.plots

def common():
	"""
	Place for common variables across whole RooPAW.

	You can attach variables to common:
	common.pi=3.14

	Delete them:
	del common.pi

	Or delete all of them:
	common()
.
	"""
	keys = list(common.__dict__.keys())
	for i in keys:
		exec('del common.'+i)

def PAWsaveNS(ip=_ip):
	for i in ip.user_ns:
		PAWsavedNS.append(i)


def PAWeval(var, ip=_ip):
	"""
	Try to evaluate expression using common.var, ip.ev(var) and gDirectory.Get(var)
	"""
	try:
		out = eval('common.'+var)
	except:
		try:
			out = ip.ev(var)
		except:
			out = gDirectory.Get(var)
	return out


def PAWstreval(var, ip=_ip):
	try:
		out = eval('common.'+var)
	except:
		try:
			out = ip.ev(var)
		except:
			out = var
	if not isinstance(out, (str, int, float)):
		out = var
	return str(out)


def PAWhelp(ip, args):
	print("""
Available magic:

	? - IPython help	

   FILES:
	tfile|hifile - open a file
	ls - list ROOT directory/file content
	close - close something
   HISTOGRAMS:
	draw|hipl - plot an object
	1d|th1f - define a histogram
	hicopy - make a copy of a histogram
	norm - normalize a histogram
	div - divide histograms
   NTUPLES:
	cut|form - define a cut|formula
	draw|ntpl - plot an ntuple formula
	%print|ntpri - print ntuple content
	scan - dump ntuple to an ASCII file
	chain - make TChain
   CANVAS:
	up - update the canvas
	zone - divide canvas into pads
   OTHER:
	exe - execute RooPAW script (kumac) or ROOT macro
	run - run [I]Python script
	opt - set an option
	reset - reset RooPAW
	paw - evaluate formula and run the result in IPython
	get - get object from gDirectory
	%< - grab output
	%> - redirect output to file
	%>> - append output to file
	%print - print magic objects
	common - place to attach global variables
	   
Run commands without any arguments or with ? suffix (e.g. zone?) to display detailed help.""")


def PAWformula(form, ip=_ip, math=0):
	"""
	Magic, math and logic formulas parser.
	"""
	if math:
		seps = ['!', '&', '|', '^', '*', '/', '(', ')', '-', '+', '=', '<', '>', ' ', ':', '${', '}', '$', '[', ']']
		tmp = form.replace('.and.', '&&').replace('.or.', '||').replace('.not.', '!')
		for sep in seps:
			tmp = tmp.replace(sep,'#'+sep+'#')
	else:
		seps = ['${','}','$']
		tmp = '@'+form.replace('}','}@')
		for sep in seps:
			tmp = tmp.replace(sep,'#')
	vset = tmp.split('#')
	out = ''
	for var in vset:
		try:
			key = var.index('@')
			evar = var.replace('@','')
		except:
			try:
				evar = float(var)
				evar = var
			except:
				evar = PAWstreval(var,ip)
				if evar != var and math:
					evar = PAWformula('('+evar+')',ip,1)
		out += evar

	return out


def PAWmakechain(mask, TTree_name, title = ''):
		out = TChain(TTree_name, title)
		files = glob.glob(mask)

		for file in files:
				out.Add(file)

		return out


def PAWpads(canvas):
	i = 0
	while canvas.GetPad(i):
		i += 1
	return i-1


def PAWnextpad():
	canvas = gPad.GetCanvas()
	pads = PAWpads(canvas)
	if pads > 0:
		try:
			pad = gPad.GetNumber()
		except:
			pad = 1
		pad = pad%pads+1
		canvas.cd(pad)
		canvas.SetSelectedPad(canvas.GetPad(pad))


def PAWpreviouspad():
	canvas = gPad.GetCanvas()
	pads = PAWpads(canvas)
	if pads > 0:
		try:
			pad = gPad.GetNumber()
		except:
			pad = 1
		pad = (pad-2)%pads+1
		canvas.cd(pad)
		canvas.SetSelectedPad(canvas.GetPad(pad))


def PAWrestorecanvas():
	global default_canvas

	try:
		canvas = gPad.GetCanvas()
		try:
			tmp = canvas.plots
		except AttributeError:
			canvas.plots = 0
			canvas.tmp = {}
		default_canvas = canvas
	except:
		default_canvas = TCanvas("default_canvas", "RooPAW canvas", 600, 600)
		default_canvas.plots = 0
		default_canvas.tmp = {}
		canvas = default_canvas

	canvas.Update()
	_ip.user_ns['canvas'] = canvas


class PAWredirector(object):
	def __init__(self, stream, filename='', mode='w'):
		import os, tempfile
		if filename:
			if PY3:
				self._file = open(filename, mode, encoding='UTF-8')
			else:
				self._file = file(filename, mode)
			self._tmp = False
		else:
			w, n = tempfile.mkstemp()
			os.unlink(n)
			try:
				self._file = os.fdopen(w, 'rw')
			except:
				self._file = os.fdopen(w, 'r+')
			self._tmp = True

		self._stream = stream
		self._savefn = os.dup(self._stream.fileno())
		os.dup2(self._file.fileno(), self._stream.fileno())

	def restore(self):
		import os
		self._stream.flush()
		os.dup2(self._savefn, self._stream.fileno())
		if self._tmp:
			self._file.seek(0)
			content = ''.join(self._file.readlines())
			self._file.close()
			del self._file, self._savefn, self._stream

			return content
		else:
			self._file.close()
			del self._file, self._savefn, self._stream



#------ magic functions
def PAWgrab(ip, args):
	"""
	out = %< command

	Grabs output of a command to the 'out' variable

	"""
	if OLD_IP:
		arg = args
	else:
		arg = args.split(' ')
		if len(arg) < 2:
			arg = False
		else:
			s = ' '
			out_var = arg[0]
			arg = s.join(arg[1:])
		
	if not arg:
		print(PAWgrab.__doc__)
		return
	
	r = PAWredirector(sys.stdout)
	run_cell(ip.shell, arg)
	ret = r.restore()
	try:
		out = int(ret)
	except ValueError:
		try:
			out = float(ret)
		except ValueError:
			out = ret

	if OLD_IP:
		return out
	else:
		ip.user_ns[out_var] = out


if not OLD_IP:
	PAWgrab.__doc__ = """
	%< out command

	Grabs output of a command to the 'out' variable
	
	"""



def PAWtofile(ip, args):
	"""
	%> filename command

	Redirects output of a command to a file
	"""
	arg = args.split(' ')
	if len(arg) < 2:
		print(PAWtofile.__doc__)
		return

	s = ' '
	r = PAWredirector(sys.stdout, PAWformula(arg[0],ip))
	run_cell(ip.shell, s.join(arg[1:]))
	r.restore()


def PAWappendtofile(ip, args):
	"""
	%>> filename command

	Appends redirected output of a command to a file
	"""
	arg = args.split(' ')
	if len(arg) < 2:
		print(PAWappendtofile.__doc__)
		return
	
	s = ' '
	r = PAWredirector(sys.stdout, PAWformula(arg[0],ip), 'a')
	run_cell(ip.shell, s.join(arg[1:]))
	r.restore()


def PAWpaw(ip, arg):
	"""
	%paw

	Evaluates magic formulas and then executing the result inside IPython
	"""
	ip = ip.shell
	if not arg:
		print(PAWpaw.__doc__)
		return
	
	ip.ex(PAWformula(arg,ip))



def PAWreset(ip, arg):
	"""
	%reset

	Reset RooPAW to the just after logon state by deleting all references created after execution of 'pawlogon.py'
	"""
	try:
		canvas = gPad.GetCanvas()
		canvas.Clear()
		canvas.Update()
		canvas.plots = 0
		canvas.tmp.clear()
	except:
		PAWrestorecanvas()
	todel = []
	ip = ip.shell
	for i in ip.user_ns:
		try:
			key = PAWsavedNS.index(i)
		except:
			todel.append(i)
	for i in todel:
		del ip.user_ns[i]


def PAWup(self, arg):
	"""
	%up

	It simply updates the canvas.
	"""
	try:
		gPad.GetCanvas().Update()
	except:
		PAWrestorecanvas()



def PAWopt(self, arg):
	"""
	%opt arg

	Sets an option passed as 'arg'.

	stat - put statistics on new plots
	nstat - hide statistics on new plots
	"""
	if arg == 'stat':
		gStyle.SetOptStat(1)
	elif arg == 'nstat':
		gStyle.SetOptStat(0)
	else:
		print(PAWopt.__doc__)



def PAWchain(ip, args):
	"""
 	%chain chain_name [mask TTree_name [title]]

	Use this command to create TChain object.

	chain_name - name of the TChain
	mask - files to be added to the chain (e.g. foo/*.root)
	TTree_name - name of the TTrees to be chained
	title - TChain title, empty by default	
	"""
	ip = ip.shell
	arg = args.split(' ')
	if not args:
		print(PAWchain.__doc__)
		return
	
	s = ' '
	try:
		for i in range(0,3):
			arg[i] = PAWstreval(arg[i],ip)
		ip.user_ns[arg[0]] = PAWmakechain(arg[1], arg[2], s.join(arg[3:]))

	except IndexError:
		ip.user_ns[arg[0]] = TChain()
	


def PAWform(ip, args):
	"""
	%form name formula
	%cut name condition

	It defines a mathematic or logic formula that can be used later as a cut condition or a function to draw using data from some ntuple (TTree). You can use both TFormula and PAW notations for logic operators. Here goes an example:

	cut cbest cl>0
	cut call cbest&&pt>.15.and.!E<0
	form p sqrt(px**2+py**2+pz**2)
	ntpl h2 sqrt(E**2-p**2) call

	where cl, pt, E, px, py, pz are variables (branches) from h2 ntuple (TTree)
	"""
	ip = ip.shell
	arg = args.split(' ', 1)
	if len(arg) < 2:
		print(PAWform.__doc__)
		return
	
	ip.user_ns[arg[0]] = PAWformula(str(arg[1]), ip)



def PAWntpl(ip, args):
	"""
	%ntpl TTree formula [cuts [[+]TH1F [options [nentries [1stentry]]]]] 
	%ntpl TTree ! cuts [+]TEventList
	%ntpl TTree ! cuts [+]TEntryList entrylist

	Plots 'formula' using the data from 'TTree' with specified 'cuts' into 'TH1F' histogram with given 'opions'. Can also make a list of entries.
	
	If you want to append data to either list of entries or a histogram precede its name with a + sign.
	"""
	ip = ip.shell
	arg = args.split(' ')
	if len(arg) < 2:
		print(PAWntpl.__doc__)
		return
	
	tree = PAWeval(PAWformula(arg[0],ip),ip)
	notebook_wait = False

	if arg[1] == '!':
		arg[1] = ''
	
	try:
		if arg[2] == '!':
			cuts = ''
		else:
			cuts = PAWformula(arg[2],ip,1)
	except:
		cuts = ''

	try:
		if arg[3] == '!':
			his = tohis = ''
		else:
			his = PAWformula(arg[3],ip)
			tohis = '>>' + his
			i = his.find('(')
			if i>-1:
				his = his[0:i]
				
			his = his.replace('+','')
	except:
		his = tohis = ''

	try:
		option = re.sub('[sS]$', 'SAME', arg[4])
		option = re.sub('[nN]$', 'GOFF', option)
		if option.count('w'):
			notebook_wait = True
			option = option.replace('w', '')
		if option == '!':
			option = ''
	except:
		option = ''

	try:
		canvas = gPad.GetCanvas()
		PAWplots = canvas.plots
	except:
		PAWrestorecanvas()
		canvas = gPad.GetCanvas()
		PAWplots = canvas.plots

	# temporary histograms
	if his != '':
		horiginal = gDirectory.Get(his)
		if horiginal:
			hname_original = horiginal.GetName()
			i = 0
			while True:
				hname = hname_original + "_old_" + str(i)
				if not gDirectory.Get(hname):
					break
				i += 1
			horiginal.SetName(hname)
			hnew = horiginal.Clone(hname_original)
			canvas.tmp[hname] = horiginal

	if re.search('SAME', option, re.IGNORECASE):
		canvas.plots -= 1
	else:
		if PAWplots:
			PAWnextpad()
		else:
			canvas.cd(1)

	try:
		entries = tree.Draw(PAWformula(arg[1],ip,1)+tohis, cuts, option, int(PAWformula(arg[5],ip)), int(PAWformula(arg[6],ip)))
	except:
		try:
			entries = tree.Draw(PAWformula(arg[1],ip,1)+tohis, cuts, option, int(PAWformula(arg[5],ip)))
		except:
			entries = tree.Draw(PAWformula(arg[1],ip,1)+tohis, cuts, option)
	if his != '':
		ip.user_ns[his] = gDirectory.Get(his)
	if not re.search('GOFF', option, re.IGNORECASE):
		canvas.Update()
		if increment_plots(canvas) == 0 and PAWnotebook and not notebook_wait:
			return canvas
	else:
		if PAWplots:
			PAWpreviouspad()
	return entries


def TTree_str(tree):
	Leaves = tree.GetListOfLeaves()
	nLeaves = Leaves.GetEntries()

	out = '%s - %s\n' % (tree.GetName(), tree.GetTitle())
	out += '\n'
	out += ' No |      Name      |                          Title\n'
	out += '----|----------------|---------------------------------------------------------\n'
	
	for i in range(nLeaves):
		out += '%3d | %-14s | %-55s\n' % (i, Leaves.At(i).GetName(), Leaves.At(i).GetBranch().GetTitle())
	
	return out

TTree.__str__ = TTree_str


def PAWprint(ip, args):
	"""
	%print something

	Magic wraper for Python's "print" command to print objects not present in the user namespace.
	"""
	ip = ip.shell
	arg = args.split(' ')
	if not args:
		print(PAWprint.__doc__)
		return

	sth = PAWeval(arg[0],ip)
	print(sth)



def PAWntpri(ip, args):
	"""
	%ntpri TTree

	Prints TTree (ntuple) list of leaves (variables).
	"""
	ip = ip.shell
	arg = args.split(' ')
	if not args:
		print(PAWntpri.__doc__)
		return
	
	tree = PAWeval(arg[0],ip)
	print(tree)


def PAWscan(ip, args):
	"""
	%scan TTree variables cuts [+]filename [options [nentries [1stentry]]]

	Dumps selected TTree (ntuple) variables to an ASCII file.
	E.g.	scan nt1 px:py:pz cl>0 nt1.dat
	
	You can append data to the file using +
	E.g.	scan nt2 px:py:pz ! +all.dat

	Or scan only some of entries
	E.g.	scan nt3 px:py:pz cl>0 nt3.dat ! 200 13
	"""
	ip = ip.shell
	arg = args.split(' ')
	if len(arg) < 3:
		print(PAWscan.__doc__)
		return
	
	tree = PAWeval(PAWformula(arg[0],ip),ip)	
	l = len(arg)
	fname = PAWformula(arg[3],ip)
	if fname[0]=='+':
		to = ' >> '
		fname = fname.replace('+','',1)
	else:
		to = ' > '
	if arg[2]=='!':
		cuts = ''
	else:
		cuts = PAWformula(arg[2],ip,1)

	try: 
		if arg[4]=='!':
			options = ''
		else:
			options = PAWformula(arg[4],ip)

		ScanArgs = [options]
		ScanArgs.append(int(PAWformula(arg[5],ip,1)))
		ScanArgs.append(int(PAWformula(arg[6],ip,1)))
	except:
		ScanArgs = ['']

	tree.GetPlayer().SetScanRedirect(1)
	tree.GetPlayer().SetScanFileName(fname+'.tmp')
	tree.Scan(PAWformula(arg[1],ip,1), cuts, *ScanArgs)

	nf = arg[1].count(':')+2

	fields = '$2'
	for i in range(3, nf+1):
		fields += ',$'+str(i)

	ip.system("cat "+fname+".tmp | sed -e 's/*/ /g; s/.*selected entries.*//' |awk '{ if (NR>3&&NF=="+str(nf)+") {print "+fields+"} }'"+to+fname)
	print(to+fname)
	ip.system('rm '+fname+'.tmp')



def PAW1d(ip, args):
	"""
	%th1f name nbinsx xlow xup [title]

	Defines a histogram with 'nbinx' bins from 'xlow' to 'xup'. It is equivalent to:
	name = TH1F(name, title, nbinsx, xlow, xup)

	You can also define histogram from a given TF1 object:
	%th1f name TF1 nbinsx xlow xup [title]
	"""
	ip = ip.shell
	arg = args.split(' ')
	if len(arg) < 4:
		print(PAW1d.__doc__)
		return
	s = ' '
	arg[0] = PAWformula(arg[0],ip)
	ip.user_ns[arg[0]] = 0 # suppress ROOT's "Potential memory leak" warning
	func = PAWeval(PAWformula(arg[1],ip),ip)
	if isinstance(func,TF1):
		for i in range(1,5):
			arg[i] = PAWformula(arg[i],ip)

		ip.user_ns[arg[0]] = TH1F(arg[0], s.join(arg[5:]), int(arg[2]), float(arg[3]), float(arg[4]))

		hist = ip.user_ns[arg[0]]
		nbinsx = int(arg[2])
		xlow = float(arg[3])
		xup = float(arg[4])

		bin = (xup-xlow)/float(nbinsx)
		for i in range(0, nbinsx+2):
			x = xlow + (i-1)*bin+bin/2.;
			hist.SetBinContent(i, func.Eval(x));
	else: 
		for i in range(1,4):
			arg[i] = PAWformula(arg[i],ip)
		ip.user_ns[arg[0]] = TH1F(arg[0], s.join(arg[4:]), int(arg[1]), float(arg[2]), float(arg[3]))



def PAWhicopy(ip, args):
	"""
	%hicopy source destination

	Equivalent to:
	destination = source.Clone('destination')
	"""
	ip = ip.shell
	arg = args.split(' ')
	if len(arg) < 2:
		print(PAWhicopy.__doc__)
		return
	
	source = PAWformula(arg[0],ip)
	name = PAWformula(arg[1],ip)
	ip.user_ns[name] = ip.user_ns[source].Clone(name)	



def PAWdiv(ip, args):
	"""
	%div first second result [c1 [c2]] [option]

	Divide histograms:
	result = (c1*first)/(c2*second)

	Equivalent to:
	result = Divide(first, second, c1, c2, option)
	"""
	ip = ip.shell
	arg = args.split(' ')
	if len(arg) < 3:
		print(PAWdiv.__doc__)
		return
	
	first = PAWformula(arg[0],ip)
	second = PAWformula(arg[1],ip)
	result = PAWformula(arg[2],ip)
	try:
		c1 = PAWformula(arg[3],ip)
	except:
		c1 = 1.
	try:
		c2 = PAWformula(arg[4],ip)
	except:
		c2 = 1.
	try:
		option = PAWformula(arg[5],ip)
	except:
		option = ''
	try:
		c1 = float(c1)
		c2 = float(c2)
	except:
		if not option:
			option = c1
		c1 = c2 = 1.
	ip.ex('%s = %s.Clone("%s")' % (result, first, result))	
	ip.ex('%s.Divide(%s, %s, %f, %f, "%s")' % (result, first, second, c1, c2, option))



def PAWtfile(ip, args):
	"""
	%tfile name filename [option [title]]

	Opens ROOT file. It is equivalent to:
	name = TFile(filename, options, title)
	"""
	ip = ip.shell
	arg = args.split(' ')
	if len(args) < 2:
		print(PAWtfile.__doc__)
		return

	s = ' '
	try:
		opt = arg[2]
		if opt == '!':
			opt = '' 
	except:
		opt = ''
	ip.user_ns[PAWformula(arg[0],ip)] = TFile(PAWformula(arg[1],ip), opt, s.join(arg[3:]))


def PAWls(ip, arg):
	"""
	%ls [name]

	Lists 'name' content or ROOT directory content if 'name' not given.
	"""
	ip = ip.shell
	arg = PAWformula(arg,ip)
	if arg == '':
		arg = gDirectory
	else:
		arg = ip.user_ns[arg]
	arg.ls()


def PAWclose(ip, args):
	"""
	%close name [option]

	Equivalent to:
	name.Close(option)
	"""
	ip = ip.shell
	arg = args.split(' ')
	if len(args) < 1:
		print(PAWclose.__doc__)
		return

	s = ' '
	try:
		opt = arg[1]
	except:
		opt = ''
	ip.user_ns[PAWformula(arg[0],ip)].Close(opt)


def PAWdraw(ip, args):
	"""
	%draw object [options]
	%draw TTree formula [cuts [[+]TH1F [options [nentries [1stentry]]]]] 
	%draw TTree ! cuts [+]TEventList
	%draw TTree ! cuts [+]TEntryList entrylist
	
	Plots object with 'options'. Equivalent to:
	object.Draw(options)

	If object is a TTree, plots 'formula' using the data from 'TTree' with specified 'cuts' into 'TH1F' histogram with given 'opions'. Can also make a list of entries.
	
	If you want to append data to either list of entries or a histogram precede its name with a + sign.

	"""
	arg = args.split(' ')
	if len(args) < 1:
		print(PAWdraw.__doc__)
		return

	hist = PAWeval(PAWformula(arg[0], ip.shell), ip.shell)
	notebook_wait = False

	if isinstance(hist, TTree):
		return PAWntpl(ip, args)

	try:
		option = re.sub('[sS]$','SAME',arg[1])
		if option.count('w'):
			notebook_wait = True
			option = option.replace('w', '')

	except:
		option = ''

	try:
		canvas = gPad.GetCanvas()
		PAWplots = canvas.plots
	except:
		PAWrestorecanvas()
		canvas = gPad.GetCanvas()
		PAWplots = canvas.plots

	if re.search('SAME', option, re.IGNORECASE):
		canvas.plots -= 1
	else:
		if PAWplots:
			PAWnextpad()
		else:
			canvas.cd(1)

	hist.Draw(option)
	canvas.Update()
	if increment_plots(canvas) == 0 and PAWnotebook and not notebook_wait:
		return canvas


def PAWnorm(ip, arg):
	"""
	%norm TH1F

	Normalizes 'TH1F' histogram.
	"""
	ip = ip.shell
	arg = PAWformula(arg,ip)
	
	if not arg:
		print(PAWnorm.__doc__)
		return
	
	ip.ex(arg+".Scale(1./"+arg+".Integral())")


re_paw_var = re.compile('\${([^}]*)}')
def run_cell(self, raw_cell, store_history=False, silent=False, shell_futures=True):
	"""Run a complete IPython cell.

		Parameters
		----------
		raw_cell : str
		  The code (including IPython code such as %magic functions) to run.
		store_history : bool
		  If True, the raw and translated cell will be stored in IPython's
		  history. For user code calling back into IPython's machinery, this
		  should be set to False.
		silent : bool
		  If True, avoid side-effects, such as implicit displayhooks and
		  and logging.  silent=True forces store_history=False.
		shell_futures : bool
		  If True, the code will share future statements with the interactive
		  shell. It will both be affected by previous __future__ imports, and
		  any __future__ imports in the code will affect the shell. If False,
		  __future__ imports are not shared in either direction.
	"""
	if (not raw_cell) or raw_cell.isspace():
		return
		
	if silent:
		store_history = False

	if OLD_IP:
		self.input_transformer_manager.push(raw_cell)
		cell = self.input_transformer_manager.source_reset()
	else: # IPython >= 7
		return self.run_cell(raw_cell, store_history, silent, shell_futures)

	# Our own compiler remembers the __future__ environment. If we want to
	# run code with a separate __future__ environment, use the default
	# compiler
	compiler = self.compile if shell_futures else CachingCompiler()

	with self.builtin_trap:
		prefilter_failed = False
		if (True): # len(cell.splitlines()) != 1:
			try:
				# use prefilter_lines to handle trailing newlines
				# restore trailing newline for ast.parse
				cell = self.prefilter_manager.prefilter_lines(cell) + '\n'

				# allow passing variables to the magic
				lines = cell.split('\n')
				cell = ''
				for line in lines:
					try:
						line.index('get_ipython().magic')
						cell += re_paw_var.sub(r"'+str(\1)+'", line)
					except ValueError:
						cell += line

					cell += '\n'
			
			except AliasError as e:
				error(e)
				prefilter_failed = True
			except Exception:
				# don't allow prefilter errors to crash IPython
				self.showtraceback()
				prefilter_failed = True

		# Store raw and processed history
		if store_history:
			self.history_manager.store_inputs(self.execution_count, cell, raw_cell)
		if not silent:
			self.logger.log(cell, raw_cell)

		if not prefilter_failed:
			# don't run if prefilter failed
			cell_name = self.compile.cache(cell, self.execution_count)

			with self.display_trap:
				try:
					code_ast = compiler.ast_parse(cell, filename=cell_name)
				except IndentationError:
					self.showindentationerror()
					if store_history:
						self.execution_count += 1
					return None
				except (OverflowError, SyntaxError, ValueError, TypeError,
						MemoryError):
					self.showsyntaxerror()
					if store_history:
						self.execution_count += 1
					return None

				code_ast = self.transform_ast(code_ast)
			
				interactivity = "none" if silent else self.ast_node_interactivity
				self.run_ast_nodes(code_ast.body, cell_name,
								   interactivity=interactivity, compiler=compiler)
				# Execute any registered post-execution functions.
				# unless we are silent
				post_exec = [] if silent else iter(self._post_execute.items())
				
				for func, status in post_exec:
					if self.disable_failing_post_execute and not status:
						continue
					try:
						func()
					except KeyboardInterrupt:
						sys.stderr.write("\nKeyboardInterrupt")
					except Exception:
						# register as failing:
						self._post_execute[func] = False
						self.showtraceback()
						sys.stderr.write('\n'.join([
							"post-execution function %r produced an error." % func,
							"If this problem persists, you can disable failing post-exec functions with:",
							"",
							"    get_ipython().disable_failing_post_execute = True"
						]))

	if store_history:
		# Write output to the database. Does nothing unless
		# history output logging is enabled.
		self.history_manager.store_output(self.execution_count)
		# Each cell is a *single* input, regardless of how many lines it has
		self.execution_count += 1
	
if not OLD_IP:
	kumacConvert = re.compile(r'(^\s*)(tfile |hifile |ls[$\s]{1}|close |draw |hipl |ntpl |1d |th1f |hicopy |norm |div |cut |form |ntpri |scan |chain |up$|zone[$\s]{1}|exe |run |opt |reset$|paw |get )')

def PAWexe(ip, arg):
	"""
	%exe filename

	Executing 'filename' script.
	"""
	ip = ip.shell
	filename = PAWformula(arg,ip)

	if not filename:
		print(PAWexe.__doc__)
		return
		
	if re.search('\.C$', filename):
		gROOT.ProcessLine('.x %s' % (filename))
	else:
		try:
			file = open(PAWformula(arg,ip))
			if PY3:
				if OLD_IP:
					kumac = file.read()
				else: # add % before RooPAW magic
					kumac = ''
					while True:
						line = file.readline()
						if not line:
							break
						kumac += kumacConvert.sub(r'\1%\2', line)
			else:
				kumac = file.read().decode('UTF-8')
			file.close()

			ip.user_ns['__file__'] = filename
			#ip.safe_execfile_ipy(filename)

			run_cell(ip, kumac, store_history=False, shell_futures=True)
		except IOError as e:
			print(e)
	if not default_canvas:
		PAWrestorecanvas()



def PAWzone(ip, args):
	"""
	%zone [x y]

	Divides canvas into pads. Defaults are x=1, y=1.
	"""
	try:
		canvas = gPad.GetCanvas()
		tmp = canvas.tmp
	except:
		PAWrestorecanvas()
		canvas = gPad.GetCanvas()
		tmp = canvas.tmp

	canvas.Clear()
	tmp.clear()
	ip = ip.shell

	if args:
		try:
			arg = args.split(' ')
			x = int(PAWformula(arg[0],ip))
			y = int(PAWformula(arg[1],ip))
		except ValueError:
			print("ValueError: Arguments must be numbers!")
			return
	else:
		x = y = 1

	if PAWnotebook:
		width = 1024 if x>1 else 512
		height = y * width / x
		canvas.SetCanvasSize(width, height)

	if x>1 or y>1:
		canvas.Divide(x, y)
		canvas.Update()
		canvas.cd(1)
		canvas.SetSelectedPad(canvas.GetPad(1))
	else:
		canvas.Update()
	
	canvas.plots = 0

	global default_canvas
	default_canvas = canvas



def PAWget(ip, args):
	"""
	%get object [name]

	Get object from gDirectory. Equivalent to:
	name = gDirectory.Get('object')
	"""
	ip = ip.shell
	arg = args.split(' ')
	if not args:
		print(PAWget.__doc__)
		return

	obj = PAWformula(arg[0],ip)
	try:
		name = PAWformula(arg[1],ip)
	except:
		name = obj
	ip.user_ns[name] = gDirectory.Get(obj)


#----
def InteractiveShell_define_magic(self, name, func):
	def func_wrap(args):
		self.shell = self
		func(self, args)
	func_wrap.__doc__ = func.__doc__
	self.register_magic_function(func_wrap, 'line_cell', name)

def load_ipython_extension(ip):
	if PAWbatch:
		ip.magic('%colors NoColor')

	try:
		ip.define_magic('<', PAWgrab)
	except:
		InteractiveShell.define_magic = InteractiveShell_define_magic
		ip.define_magic('<', PAWgrab)

	ip.define_magic('>', PAWtofile)
	ip.define_magic('>>', PAWappendtofile)
	ip.define_magic('paw', PAWpaw)
	ip.define_magic('reset', PAWreset)
	ip.define_magic('up', PAWup)
	ip.define_magic('opt', PAWopt)
	ip.define_magic('chain', PAWchain)
	ip.define_magic('form', PAWform)
	ip.define_magic('cut', PAWform)
	ip.define_magic('ntpl', PAWntpl)
	ip.define_magic('ntpri', PAWntpri)
	ip.define_magic('scan', PAWscan)
	ip.define_magic('ntscan', PAWscan)
	ip.define_magic('1d', PAW1d)
	ip.define_magic('th1f', PAW1d)
	ip.define_magic('hicopy', PAWhicopy)
	ip.define_magic('div', PAWdiv)
	ip.define_magic('hifile', PAWtfile)
	ip.define_magic('tfile', PAWtfile)
	ip.define_magic('ls', PAWls)
	ip.define_magic('close', PAWclose)
	ip.define_magic('hipl', PAWdraw)
	ip.define_magic('draw', PAWdraw)
	ip.define_magic('norm', PAWnorm)
	ip.define_magic('exe', PAWexe)
	ip.define_magic('zone', PAWzone)
	ip.define_magic('get', PAWget)
	ip.define_magic('print', PAWprint)

	ip.define_magic('PAWhelp', PAWhelp)

	ip.user_ns['c1'] = default_canvas
	ip.user_ns['canvas'] = default_canvas
	ip.user_ns['_ip'] = _ip

	#--- backward compatibility
	ip.ex("from ROOT import TChain, TCanvas, TH1F, TTree, TFile, gDirectory, gStyle, TF1, TMath, gROOT, gPad, kRed, kGreen, kBlue, kYellow, TGraph")
	ip.ex("from RooPAW import PAWformula, common, PAWsaveNS")

	#---- exe pawlogon.py
	ip.magic('exe pawlogon.py')

	PAWsaveNS(ip)

	#--- exe file passed as a parameter
	if PAWscript:
		ip.magic('exe ' + PAWscript)
	elif not PAWbatch:
		print("\nIf you need help issue " + CSI + "34mPAWhelp" + CSI + "0m command.")
