# [ROOT based Physics Analysis Workstation](http://belle2.ifj.edu.pl/roopaw)

## What's that?

It is a set of magic commands that extends [IPython](http://ipython.org) shell. The goal is to make physics analysis with [ROOT](http://root.cern.ch) much more user friendly.

## Installation

To install the newest version of RooPAW run the following line in your terminal:

```
curl -fsSL https://gitlab.com/uxmaster/roopaw/snippets/1991667/raw | sh
```
